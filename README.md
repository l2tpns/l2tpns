# Layer 2 tunneling protocol network server (LNS)

l2tpns notably supports:

- radius authentication/authorization
- accounting
- clustered load balancing and fail-over
- MPPP
- BGP
- kernel-level acceleration
- throttling
- snooping
- walled garden
- plugins
- a management cli

* Mailing list: <https://lists.ffdn.org/wws/info/l2tpns>
* Code: <https://code.ffdn.org/l2tpns/l2tpns>
