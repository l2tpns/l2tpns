% STARTUP-CONFIG(5) Configuration file for l2tpns
% l2tpns developers <l2tpns@lists.ffdn.org>
% January 31, 2021

# NAME

startup-config - configuration file for l2tpns

# SYNOPSIS

/etc/l2tpns/startup-config

# DESCRIPTION

**startup-config** is the configuration file for **l2tpns**

The format is plain text, in the same format as accepted by
the configuration mode of l2tpns's telnet administrative
interface. Comments are indicated by either the character # or !.

## SETTINGS

Settings are specified with

**set** `variable` `value`

A list of the possible configuration directives follows. Each of these should be set by a line like:

**set** _configstring_ _"value"_  
**set** _ipaddress_ _192.168.1.1_  
**set** _boolean_ _true_  

The following `variables` may be set:

**accounting\_dir** (string)

    If set to a directory, then every 5 minutes the current usage for every connected use will be dumped to a file in this directory. Each file dumped begins with a header, where each line is prefixed by #. Following the header is a single line for every connected user, fields separated by a space.

    The fields are username, ip, qos, uptxoctets, downrxoctets, origin (optional). The qos field is 1 if a standard user, and 2 if the user is throttled. The origin field is dump if account_all_origin is set to true (origin value: L=LAC data, R=Remote LNS data, P=PPPOE data).

**account\_all\_origin** (boolean)

    If set to true, all origin of the usage is dumped to the accounting file (LAC+Remote LNS+PPPOE)(default false).

**allow\_duplicate\_users** (boolean)

    Allow multiple logins with the same username. If false (the default), any prior session with the same username will be dropped when a new session is established.

**auth\_tunnel\_change\_addr\_src** (boolean)

    This parameter authorize to change the source IP of the tunnels l2tp. This parameter can be used when the remotes BAS/LAC are l2tpns server configured in cluster mode, but that the interface to remote LNS are not clustered (the tunnel can be coming from different source IP) (default: no).

**bind\_address** (ip address)

    It's the listen address of the l2tp udp protocol sent and received to LAC. This address is also assigned to the tun interface if no iftun_address is specified. Packets containing user traffic should be routed via this address if given, otherwise the primary address of the machine.

**bind\_multi\_address** (ip address)

    This parameter permit one to listen several address of the l2tp udp protocol (and set several address to the tun interface).

    WHEN this parameter is set, It OVERWRITE the parameters "bind_address" and "iftun_address".

    these can be interesting when you want do load-balancing in cluster mode of the uploaded from the LAC. For example you can set a bgp.prepend(MY_AS) for Address1 on LNS1 and a bgp.prepend(MY_AS) for Address2 on LNS2 (see BGP AS-path prepending).

    example of use with 2 address:

    set bind_multi_address "64.14.13.41, 64.14.13.42"

**cluster\_address** (ip address)

    Multicast cluster address (default: 239.192.13.13). See the section on Clustering for more information.

**cluster\_port** (int)

    UDP cluster port (default: 32792). See the section on Clustering for more information.

**cluster\_interface** (string)

    Interface for cluster packets (default: eth0).

**cluster\_mcast\_ttl** (int)

    TTL for multicast packets (default: 1).

**cluster\_hb\_interval** (int)

    Interval in tenths of a second between cluster heartbeat/pings.

**cluster\_hb\_timeout** (int)

    Cluster heartbeat timeout in tenths of a second. A new master will be elected when this interval has been passed without seeing a heartbeat from the master.

**cluster\_master\_min\_adv** (int)

    Determines the minimum number of up to date slaves required before the master will drop routes (default: 1).

**debug** (int)

    Set the level of debugging messages written to the log file. The value should
    be between 0 and 5, with 0 being no debugging, and 5 being the highest.
    A rough description of the levels is:

    - 0. Critical Errors - Things are probably broken
    - 1. Errors - Things might have gone wrong, but probably will recover
    - 2. Warnings - Just in case you care what is not quite perfect
    - 3. Information - Parameters of control packets
    - 4. Calls - For tracing the execution of the code
    - 5. Packets - Everything, including a hex dump of all packets processed... probably twice

    Note that the higher you set the debugging level, the slower the program will run. Also, at level 5 a LOT of information will be logged. This should only ever be used for working out why it doesn't work at all.

**dump\_speed** (boolean)

    If set to true, then the current bandwidth utilization will be logged every second. Even if this is disabled, you can see this information by running the uptime command on the CLI.

**disable\_sending\_hello** (boolean)

    Disable l2tp sending HELLO message for Apple compatibility. Some OS X implementation of l2tp no manage the L2TP "HELLO message". (default: no).

**echo\_timeout** (int)

    Time between last packet sent and LCP ECHO generation (default: 10 (seconds)).

**guest\_account**

    Allow multiple logins matching this specific username.

**icmp\_rate** (int)

    Maximum number of host unreachable ICMP packets to send per second.

**idle\_echo\_timeout** (int)

    Drop sessions who have not responded within idle_echo_timeout seconds (default: 240 (seconds))

**iftun\_address** (ip address)

    This parameter is used when you want a tun interface address different from the address of "bind_address" (For use in cases of specific configuration). If no address is given to iftun_address and bind_address, 1.1.1.1 is used.

**l2tp\_mtu** (int)

    MTU of interface for L2TP traffic (default: 1500). Used to set link MRU and adjust TCP MSS.

**mp\_mrru** (int)

    MRRU for MP traffic (default: 1614). Can be set to 0 to disable MP negociation.

**l2tp\_secret** (string)

  The secret used by l2tpns for authenticating tunnel request. Must be the same as the LAC, or authentication will fail. Only actually be used if the LAC requests authentication.

**lock\_pages** (boolean)

  Keep all pages mapped by the l2tpns process in memory.

**log\_file** (string)

  This will be where all logging and debugging information is written to.This may be either a filename, such as /var/log/l2tpns, or the string syslog:facility, where facility is any one of the syslog logging facilities, such as local5.

**multi\_read\_count** (int)

  Number of packets to read off each of the UDP and TUN fds when returned as readable by select (default: 10). Avoids incurring the unnecessary system call overhead of select on busy servers.

**packet\_limit** (int>

  Maximum number of packets of downstream traffic to be handled each tenth of a second per session. If zero, no limit is applied (default: 0). Intended as a DoS prevention mechanism and not a general throttling control (packets are dropped, not queued).

**peer\_address** (ip address)

  Address to send to clients as the default gateway.

**pid\_file** (string)

  If set, the process id will be written to the specified file. The value must be an absolute path.

**ppp\_keepalive** (boolean)

  Change this value to no to force generation of LCP ECHO every echo\_timeout seconds, even there are activity on the link (default: yes)

**ppp\_restart\_time** (int)
**ppp\_max\_configure** (int)
**ppp\_max\_failure** (int)

  PPP counter and timer values, as described in Section 4.1 of RFC1661.

  _ppp\_restart\_time_, Restart timer for PPP protocol negotiation in seconds (default: 3).

  _ppp\_max\_configure_, Number of configure requests to send before giving up (default: 10).

  _ppp\_max\_failure_, Number of Configure-Nak requests to send before sending a Configure-Reject (default: 5).

**primary\_dns** (ip address), **secondary\_dns** (ip address)

  Whenever a PPP connection is established, DNS servers will be sent to the user, both a primary and a secondary. If either is set to 0.0.0.0, then that one will not be sent.

**primary\_radius** (ip address), **secondary\_radius** (ip address)

  Sets the RADIUS servers used for both authentication and accounting. If the primary server does not respond, then the secondary RADIUS server will be tried.

  Note: in addition to the source IP address and identifier, the RADIUS server must include the source port when detecting duplicates to suppress (in order to cope with a large number of sessions coming on-line simultaneously l2tpns uses a set of udp sockets, each with a separate identifier).

**primary\_radius\_port** (short), **secondary\_radius\_port** (short)

  Sets the authentication ports for the primary and secondary RADIUS servers. The accounting port is one more than the authentication port. If no RADIUS ports are given, the authentication port defaults to 1812, and the accounting port to 1813.

**radius\_accounting** (boolean)

  If set to true, then RADIUS accounting packets will be sent. This means that a **Start** record will be sent when the session is successfully authenticated, and a **Stop** record will be sent when the session is closed.

**radius\_interim** (int)

  If radius\_accounting is on, defines the interval between sending of RADIUS interim accounting records (in seconds).

**radius\_secret** (string)

  This secret will be used in all RADIUS queries. If this is not set then RADIUS queries will fail.

**radius\_require\_message\_authenticator** (string)

  If set to true, RADIUS answers to AccessRequests will have to contain
  a valid MessageAuthenticator.
  If set to auto (default), if the first RADIUS answer to AccessRequests
  contains a valid MessageAuthenticator, subsequent answers will have to
  contain one.
  If set to no (not recommended), RADIUS answers to AccessRequests do not have
  to contain a valid MessageAuthenticator.
  It is advised to set this to true after checking that your RADIUS server
  does send MessageAuthenticator.

**radius\_authtypes** (string)

  A comma separated list of supported RADIUS authentication methods ("pap" or "chap"), in order of preference (default "pap").

**radius\_dae\_port** (short)

  Port for DAE RADIUS (Packet of Death/Disconnect, Change of Authorization) requests (default: 3799).

**radius\_bind\_min**, **radius\_bind\_max** (int)

  Define a port range in which to bind sockets used to send and receive RADIUS packets. Must be at least RADIUS\_FDS (64) wide. Simplifies firewalling of RADIUS ports (default: dynamically assigned).

**random\_device** (string)

  Path to random data source (default /dev/urandom). Use "" to use the rand() library function.

**scheduler\_fifo** (boolean)

  Sets the scheduling policy for the l2tpns process to SCHED\_FIFO. This causes the kernel to immediately preempt any currently running SCHED\_OTHER (normal) process in favour of l2tpns when it becomes runnable. Ignored on uniprocessor systems.

**send\_garp** (boolean)

  Determines whether or not to send a gratuitous ARP for the bind\_address when the server is ready to handle traffic (default: true). This value is ignored if BGP is configured.

**tundevicename** (string)

  Name of the tun interface (default: "tun0").

**throttle\_speed** (int)

  Sets the default speed (in kbits/s) which sessions will be limited to. If this is set to 0, then throttling will not be used at all. Note: You can set this by the CLI, but changes will not affect currently connected users.

**throttle\_buckets** (int)

  Number of token buckets to allocate for throttling. Each throttled session requires two buckets (in and out).

**kernel\_accel** (boolean)

  Determines whether or not to enable kernel acceleration. Note that only one l2tpns instance can use it per network namespace, otherwise they will step on each other. Also, if you have a lot of clients (e.g. at least a hundred), listening for DHCPv6 and RS requires a lot of igmp6 subscriptions, tuning sysctl may be needed, such as

sysctl net.core.optmem\_max=1048576

otherwise the logs will mention failures to subscribe due to lack of memory.

## DHCPv6 And IPv6 SETTINGS

**dhcp6\_preferred\_lifetime** (int)

  The preferred lifetime for the IPv6 address and the IPv6 prefix address, expressed in units of seconds (see rfc3315).

**dhcp6\_valid\_lifetime** (int)

  The valid lifetime for the IPv6 address and the IPv6 prefix address, expressed in units of seconds (see rfc3315).

**dhcp6\_server\_duid** (int)

  DUID Based on Link-layer Address (DUID-LL) (see rfc3315).

**primary\_ipv6\_dns**, **secondary\_ipv6\_dns** (Ipv6 address)

  IPv6 DNS servers will be sent to the user (see rfc3646).

**default\_ipv6\_domain\_list** (string)

  The Domain Search List (ex: "fdn.fr") (see rfc3646).

**ipv6\_prefix** (Ipv6 address)

  Enable negotiation of IPv6. This forms the the first 64 bits of the client allocated address. The remaining 64 come from the allocated IPv4 address and 4 bytes of 0.

## LAC SETTINGS

**bind\_address\_remotelns** (ip address)

  Address of the interface to listen the remote LNS tunnels. If no address is given, all interfaces are listened (Any Address).

**bind\_portremotelns** (short)

  Port to bind for the Remote LNS (default: 65432).

A static REMOTES LNS configuration can be entered by the command:

**setforward** _MASK_ _IP_ _PORT_ _SECRET_

  where MASK specifies the mask of users who have forwarded to remote LNS (ex: "/friendISP@company.com").

  where IP specifies the IP of the remote LNS (ex: "66.66.66.55").

  where PORT specifies the L2TP Port of the remote LNS (Normally should be 1701) (ex: 1701).

  where SECRET specifies the secret password the remote LNS (ex: mysecret).

The static REMOTE LNS configuration can be used when the friend ISP not have a proxied Radius.

If a proxied Radius is used, It will return the RADIUS attributes:

Tunnel-Type:1 = L2TP  
Tunnel-Medium-Type:1 = IPv4  
Tunnel-Password:1 = "LESECRETL2TP"  
Tunnel-Server-Endpoint:1 = "88.xx.xx.x1"  
Tunnel-Assignment-Id:1 = "friendisp\_lns1"  
Tunnel-Type:2 += L2TP  
Tunnel-Medium-Type:2 += IPv4  
Tunnel-Password:2 += "LESECRETL2TP"  
Tunnel-Server-Endpoint:2 += "88.xx.xx.x2"  
Tunnel-Assignment-Id:2 += "friendisp\_lns2"  

## PPPOE SETTINGS

**pppoe\_if\_to\_bind** (string)

  PPPOE server interface to bind (ex: "eth0.12"), If not specified the server PPPOE is not enabled. For the pppoe clustering, all the interfaces PPPOE of the clusters must use the same HW address (MAC address).

**pppoe\_service\_name** (string)

  PPPOE service name (default: NULL).

**pppoe\_ac\_name** (string)

  PPPOE access concentrator name (default: "l2tpns-pppoe").

**pppoe\_only\_equal\_svc\_name** (boolean)

  If set to yes, the PPPOE server only accepts clients with a "service-name" different from NULL and a "service-name" equal to server "service-name" (default: no).

## BGP ROUTING

The routing configuration section is entered by the command

**router** **bgp** _as_

where _as_ specifies the local AS number.

Subsequent lines prefixed with **neighbour** _peer_ define the attributes of BGP neighhbours. Valid commands are:

**neighbour** _peer_ **remote-as** _as_

**neighbour** _peer_ **timers** _keepalive_ _hold_

Where _peer_ specifies the BGP neighbour as either a hostname or IP address, _as_ is the remote AS number and _keepalive_, _hold_ are the timer values in seconds.

## NAMED ACCESS LISTS

Named access lists may be defined with either of

**ip** **access-list** **standard** _name_  
**ip** **access-list** **extended** _name_

Subsequent lines starting with permit or deny define the body of the access-list.

### Standard Access Lists

Standard access lists are defined with:

{**permit**|**deny**} _source_ \[_dest_\]

Where _source_ and _dest_ specify IP matches using one of:

_address_ _wildard_  
**host** _address_  
**any**

_address_ and _wildard_ are in dotted-quad notation, bits in the _wildard_ indicate which address bits in _address_ are relevant to the match (0 = exact match; 1 = don't care).

The shorthand 'host address' is equivalent to '_address_ **0.0.0.0**'; '**any**' to '**0.0.0.0** **255.255.255.255**'.

### Extended Access Lists

Extended access lists are defined with:

{**permit**|**deny**} _proto_ _source_ \[_ports_\] _dest_ \[_ports_\] \[_flags_\]

Where _proto_ is one of **ip**, **tcp** or **udp**, and _source_ and _dest_ are as described above for standard lists.

For TCP and UDP matches, source and destination may be optionally followed by a ports specification:

{**eq|neq|gt|lt**} _port_  
**range** _from_ _to_

_flags_ may be one of:

{**match-any|match-all**} {**+|-**}{**fin|syn|rst|psh|ack|urg**} ...

    Match packets with any or all of the tcp flags set (+) or clear (-).

**established**

    Match "established" TCP connections: packets with RST or ACK set, and SYN clear.

**fragments**

    Match IP fragments. May not be specified on rules with layer 4 matches.

# SEE ALSO

l2tpns(8), nsctl(8)
